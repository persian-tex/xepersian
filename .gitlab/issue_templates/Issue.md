<!---
!! Please fill out all sections !!
-->

## Brief outline of the issue



## Minimal example showing the issue

```tex
\documentclass{article}            % or some other class

  % Any packages other than the xepersian package must be loaded here

  % The xepersian package must be loaded as the last package
\usepackage[%
    % Any xepersian package option goes here
]{xepersian}
\settextfont{Yas}

  % Any preamble code goes here
  
\begin{document}

  % Demonstration of issue here
  
\end{document}
```


## Log and PDF files 

<!---
!! Use drag-and-drop !!
-->
